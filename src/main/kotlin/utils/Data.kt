package utils

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.date

/**
 * Created by Mihael Valentin Berčič
 * on 10/09/2020 at 12:42
 * using IntelliJ IDEA
 */

data class LoginData(val email: String, val password: String)
data class OpinionData(val movie: Int, val liked: Boolean)

data class MovieData(
    val id: Int,
    val title: String,
    val duration: Int,
    val director: String,
    val description: String,
    val image: String,
    val genre: String,
    val cast: MutableList<String> = mutableListOf(),
    var liked: Boolean = false
)

data class MovieResponse(
    val forYou: List<MovieData>,
    val new: List<MovieData>,
    val liked: List<MovieData>
)

object Cast : IntIdTable("Cast") {
    val name = varchar("name", 255)
    val surname = varchar("surname", 255)
    val movieId = integer("movie_id")
}

object Director : IntIdTable("Director") {
    val name = varchar("name", 255)
    val surname = varchar("surname", 255)
    val born = date("born")
}

object Genre : IntIdTable("Genre") {
    val description = varchar("description", 255)
}

object MovieDetails : IntIdTable("MovieDetails") {
    val movieId = integer("movie_id")
    val directorId = integer("director_id")
    val description = varchar("description", 255)
    val genreId = integer("genre_id")
    val released = date("released")
    val image = varchar("image", 255)
}

object MovieRatings : IntIdTable("MovieRatings") {
    val movieId = integer("movie_id")
    val critic = varchar("critic", 255)
    val value = double("value")
    val updated = date("updated")
}

object Movies : IntIdTable("Movies") {
    val title = varchar("title", 255)
    val duration = integer("duration")
    val added = date("added")
}

object Opinions : IntIdTable("Opinions") {
    val userId = integer("user_id")
    val movieId = integer("movie_id")
    val value = bool("value")
}

object User : IntIdTable("User") {
    val email = varchar("email", 255)
    val password = varchar("password", 255)
    val salt = varchar("salt", 255)
    val isModerator = bool("is_moderator")
}
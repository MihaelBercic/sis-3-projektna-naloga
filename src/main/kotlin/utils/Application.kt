package utils

import com.google.gson.GsonBuilder
import io.javalin.Javalin
import io.javalin.http.staticfiles.Location
import org.jetbrains.exposed.sql.Database

/**
 * Created by Mihael Valentin Berčič
 * on 10/09/2020 at 12:00
 * using IntelliJ IDEA
 */
object Application {

    const val token = "1249ztrsdtfzguinjmksflkdjshagzvfghbjn"

    val server = Javalin.create {
        it.showJavalinBanner = false
        it.addStaticFiles("./web", Location.EXTERNAL)
    }.start(6969)

    val gson = GsonBuilder().setPrettyPrinting().create()

    /**
     *     Database.connect("jdbc:sqlite:items.db", "org.sqlite.JDBC")
    TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE
    transaction { SchemaUtils.createMissingTablesAndColumns(Products, Images) }

     */
}
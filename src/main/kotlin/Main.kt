import io.javalin.http.Context
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import utils.*
import utils.Cast
import java.sql.Connection
import kotlin.math.absoluteValue

/**
 * Created by Mihael Valentin Berčič
 * on 10/09/2020 at 11:56
 * using IntelliJ IDEA
 */

/**
 * Purely University project oriented and thus no real safety, hashing or encryption is involved.
 */

fun main() {

    Database.connect(
        "jdbc:mysql://95.179.131.179:3306/projekt?useSSL=false",
        driver = "com.mysql.jdbc.Driver",
        user = "miha",
        password = "miha"
    )

    TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

    Application.server.exception(Exception::class.java) { exception, _ -> exception.printStackTrace() }

    Application.server.before {
        val path = it.path()
        if (path.contains("html")) {
            val cookie = it.cookie("login")
            if (cookie != Application.token && !path.contains("index")) it.redirect("/index.html")
        }
    }

    "login" post {
        bodyAs<LoginData>()?.apply {
            if (email == "miha@gmail.com" && password == "miha") {
                cookie("login", Application.token)
                redirect("/main.html", 301)
            } else error("Nope")
        }
    }

    "movie-opinion" post {
        bodyAs<OpinionData>()?.apply {
            println(this)
            if (liked) {
                println("liking a movie!")
                transaction {
                    Opinions.insert {
                        it[movieId] = movie
                        it[userId] = 1
                        it[value] = true
                    }
                }
            } else {
                println("Deleting liked movie...")
                transaction {
                    Opinions.deleteWhere {
                        (Opinions.movieId eq movie)
                            .and(Opinions.userId eq 1)
                    }

                }
            }
        }

    }

    "movies" post {
        transaction {
            val userId = 1
            val allMovies = Movies
                .join(MovieDetails, JoinType.LEFT, Movies.id, MovieDetails.movieId)
                .join(MovieRatings, JoinType.LEFT, Movies.id, MovieRatings.movieId)
                .join(Director, JoinType.LEFT, MovieDetails.directorId, Director.id)
                .join(Genre, JoinType.LEFT, MovieDetails.genreId, Genre.id)
                .selectAll().map(ResultRow::toMovie)

            Movies.join(Cast, JoinType.LEFT, Movies.id, Cast.movieId).selectAll().forEach {
                allMovies[it[Movies.id].value - 1].cast.add("${it[Cast.name]} ${it[Cast.surname]}")
            }

            val likedMovies =
                Opinions.select { Opinions.userId eq userId }.map { allMovies[it[Opinions.movieId].absoluteValue - 1] }

            likedMovies.forEach { it.liked = true }
            val suggestedMovies = allMovies.filter { movie -> likedMovies.any { it.genre == movie.genre } }

            result(
                Application.gson.toJson(
                    MovieResponse(
                        suggestedMovies,
                        allMovies,
                        likedMovies
                    )
                )
            )

        }
    }


}

infix fun String.post(context: Context.() -> Unit) = Application.server.post(this, context)
infix fun String.get(context: Context.() -> Unit) = Application.server.get(this, context)
inline fun <reified T> Context.bodyAs(): T? = Application.gson.fromJson<T>(body(), T::class.java)

private fun ResultRow.toMovie() = MovieData(
    this[Movies.id].value,
    this[Movies.title],
    this[Movies.duration],
    "${this[Director.name]} ${this[Director.surname]}",
    this[MovieDetails.description],
    this[MovieDetails.image],
    this[Genre.description]
)
$(document).ready(() => {

    $("#login-button").on('click', () => {
        const email = $("#email").val()
        const password = $("#password").val()

        const data = {email: email, password: password}
        fetch("http://localhost:6969/login", {
            method: "POST",
            redirect: "follow",
            body: JSON.stringify(data)
        }).then(res => {
            if (res.status !== 200) alert("Your credentials do not match with anything in our system.")
            else if (res.redirected) window.location = res.url
        })
    })

    $("#close").on('click', toggleDescription)

    $("#movie-like").on('click', () => {
        currentMovie.liked = !currentMovie.liked
        fetch("http://localhost:6969/movie-opinion", {
            method: "POST",
            body: JSON.stringify({movie: currentMovie.id, liked: currentMovie.liked})
        })
        refreshHeart()
    })

    loadMovies();

})

var forYou = [];
var fresh = [];
var liked = [];

function loadMovies() {
    fetch("http://localhost:6969/movies", {
        method: "POST"
    }).then(res => {
        res.json().then(json => {
            const recommended = json.forYou;
            const newMovies = json.new;
            const likedMovies = json.liked;

            forYou = recommended;
            fresh = newMovies;
            liked = likedMovies;

            const recommendedList = document.getElementById("recommended")
            const freshList = document.getElementById("fresh")
            const likedList = document.getElementById("liked")

            recommendedList.innerHTML = "";
            freshList.innerHTML = "";
            likedList.innerHTML = "";

            recommended.forEach((movie, index) => {
                const newMovie = document.createElement("li");
                newMovie.id = movie.id;
                newMovie.className = "movie-card";
                newMovie.style = `background-image: url("${movie.image}")`;
                recommendedList.append(newMovie);
            })

            newMovies.forEach((movie, index) => {
                const newMovie = document.createElement("li");
                newMovie.id = movie.id;
                newMovie.className = "movie-card";
                newMovie.style = `background-image: url("${movie.image}")`;
                freshList.append(newMovie);
            })

            liked.forEach((movie, index) => {
                const newMovie = document.createElement("li");
                newMovie.id = movie.id;
                newMovie.className = "movie-card";
                newMovie.style = `background-image: url("${movie.image}")`;
                likedList.append(newMovie);
            })


            setClickListener();

        })

    })
}

var show = false;
var currentMovie;

function toggleDescription() {
    show = !show;
    const element = document.getElementById("movie-description");
    $(element).css({"top": `${show ? 20 : 600}px`})
}

function setClickListener() {
    $(".movie-card").on('click', (element) => {
        const target = $(element.target)[0];
        const id = target.id;
        const movie = fresh[id - 1];
        currentMovie = movie;

        document.getElementById("movie-title").innerHTML = movie.title;
        document.getElementById("movie-poster").src = movie.image;
        document.getElementById("movie-director").innerHTML = `<b>Director: </b>${movie.director}`
        document.getElementById("movie-cast").innerHTML = `<b>Cast: </b>${movie.cast}`
        document.getElementById("movie-info").innerHTML = movie.description
        document.getElementById("movie-genre").innerHTML = `<b>Genre: </b>${movie.genre}`
        refreshHeart()
        toggleDescription();

    })
}

function refreshHeart() {
    document.getElementById("movie-like").src = currentMovie.liked ? "css/filled.png" : "css/not-filled.png";
    loadMovies()
}

function logout(){
    document.cookie = "login= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
}
-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 10, 2020 at 02:41 PM
-- Server version: 5.7.27-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `SISIII2020_89181149`
--

-- --------------------------------------------------------

--
-- Table structure for table `Cast`
--

CREATE TABLE `Cast` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `movie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Cast`
--

INSERT INTO `Cast` (`id`, `name`, `surname`, `movie_id`) VALUES
(1, 'Dwayne', 'Johnson', 8),
(2, 'Kevin', 'Hart', 8),
(3, 'Danielle', 'Nicolet', 8),
(4, 'Jonah', 'Hill', 7),
(5, 'Miles', 'Teller', 7),
(6, 'Steve', 'Lantz', 7),
(7, 'Dave', 'Bautista', 6),
(8, 'Kumail', 'Nanijani', 6),
(9, 'Mira', 'Sorvino', 6),
(10, 'Chris', 'Hemsworth', 5),
(11, 'Bryon', 'Lerum', 5),
(12, 'Ryder', 'Lerum', 5),
(13, 'Zach', 'Galifianakis', 4),
(14, 'Bradley', 'Cooper', 4),
(15, 'Justin', 'Bartha', 4),
(16, 'Anna Maria', 'Sieklucka', 3),
(17, 'Michele', 'Morrone', 3),
(18, 'Bronislaw', 'Wroclawski', 3),
(19, 'Suraj', 'Sharma', 2),
(20, 'Irrfan', 'Khan', 2),
(21, 'Adil', 'Hussain', 2),
(22, 'Jason', 'Momoa', 1),
(23, 'Amber', 'Heard', 1),
(24, 'Williem', 'Dafoe', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Director`
--

CREATE TABLE `Director` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `born` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Director`
--

INSERT INTO `Director` (`id`, `name`, `surname`, `born`) VALUES
(1, 'David', 'Crane', '1950-08-09'),
(2, 'Marta', 'Kauffman', '1961-05-02'),
(3, 'Scott', 'Silveri', '1970-03-03'),
(4, 'Jeff', 'Greenstein', '1933-04-12'),
(5, 'Alexa', 'Junge', '1989-06-07');

-- --------------------------------------------------------

--
-- Table structure for table `Genre`
--

CREATE TABLE `Genre` (
  `id` int(8) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Genre`
--

INSERT INTO `Genre` (`id`, `description`) VALUES
(1, 'Comedy'),
(2, 'Action'),
(3, 'Drama'),
(4, 'Adventure'),
(5, 'Fantasy'),
(6, 'Crime'),
(7, 'Romance');

-- --------------------------------------------------------

--
-- Table structure for table `MovieDetails`
--

CREATE TABLE `MovieDetails` (
  `id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `director_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `genre_id` int(255) NOT NULL,
  `released` date NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `MovieDetails`
--

INSERT INTO `MovieDetails` (`id`, `movie_id`, `director_id`, `description`, `genre_id`, `released`, `image`) VALUES
(1, 1, 1, 'Aquaman is a 2018 American superhero film. Distributed by Warner Bros. Pictures, it is the sixth film in the DC Extended Universe (DCEU).', 5, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BOTk5ODg0OTU5M15BMl5BanBnXkFtZTgwMDQ3MDY3NjM@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(2, 2, 2, 'A young man who survives a disaster at sea is hurtled into an epic journey of adventure and discovery. While cast away, he forms an unexpected connection with another survivor: a fearsome Bengal tiger.', 6, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BNTg2OTY2ODg5OF5BMl5BanBnXkFtZTcwODM5MTYxOA@@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(9, 3, 3, 'Massimo is a member of the Sicilian Mafia family and Laura is a sales director. She does not expect that on a trip to Sicily trying to save her relationship, Massimo will kidnap her and give her 365 days to fall in love with him.', 3, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BODljZTM3ODAtMDc0YS00NmI4LTlmZTUtM2I5MDAzNTQxZmMxXkEyXkFqcGdeQXVyMTEwMTY3NDI@._V1_UY268_CR4,0,182,268_AL_.jpg'),
(10, 4, 4, 'Three buddies wake up from a bachelor party in Las Vegas, with no memory of the previous night and the bachelor missing. They make their way around the city in order to find their friend before his wedding.', 0, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BNGQwZjg5YmYtY2VkNC00NzliLTljYTctNzI5NmU3MjE2ODQzXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(11, 5, 5, 'Tyler Rake, a fearless black market mercenary, embarks on the most deadly extraction of his career when he\'s enlisted to rescue the kidnapped son of an imprisoned international crime lord.', 2, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(12, 6, 1, 'A detective recruits his Uber driver into an unexpected night of adventure.', 4, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BOGE1ZjFhYzAtYWM4ZC00NGI1LWFmYzMtZWRhZDhjMjE4YzBjXkEyXkFqcGdeQXVyODQzNTE3ODc@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(13, 7, 3, 'Loosely based on the true story of two young men, David Packouz and Efraim Diveroli, who won a three hundred million dollar contract from the Pentagon to arm America\'s allies in Afghanistan.', 3, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BMjEyNzQ0NzM4MV5BMl5BanBnXkFtZTgwMDI0ODM2OTE@._V1_UX182_CR0,0,182,268_AL_.jpg'),
(14, 8, 2, 'After he reconnects with an awkward pal from high school through Facebook, a mild-mannered accountant is lured into the world of international espionage.\r\n EN', 6, '2020-09-01', 'https://m.media-amazon.com/images/M/MV5BMjA2NzEzNjIwNl5BMl5BanBnXkFtZTgwNzgwMTEzNzE@._V1_UX182_CR0,0,182,268_AL_.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `MovieRatings`
--

CREATE TABLE `MovieRatings` (
  `id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `critic` varchar(255) NOT NULL,
  `value` double NOT NULL,
  `updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `MovieRatings`
--

INSERT INTO `MovieRatings` (`id`, `movie_id`, `critic`, `value`, `updated`) VALUES
(1, 1, 'IBDM', 7.2, '2020-09-01'),
(2, 2, 'IBDM', 8.8, '2020-09-01'),
(3, 3, 'IBDM', 7.4, '2020-09-01'),
(4, 4, 'IBDM', 6.7, '2020-09-01'),
(5, 5, 'IBDM', 9.2, '2020-09-01'),
(6, 6, 'IBDM', 7.9, '2020-09-01'),
(7, 7, 'IBDM', 9.9, '2020-09-01'),
(8, 8, 'IBDM', 9.3, '2020-09-01');

-- --------------------------------------------------------

--
-- Table structure for table `Movies`
--

CREATE TABLE `Movies` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `duration` int(11) NOT NULL,
  `added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Movies`
--

INSERT INTO `Movies` (`id`, `title`, `duration`, `added`) VALUES
(1, 'Aquaman', 144, '2020-09-01'),
(2, 'Life of Pi', 127, '2020-09-01'),
(3, '365 Days', 128, '2020-09-01'),
(4, 'Hungover', 98, '2020-09-01'),
(5, 'Extraction', 132, '2020-09-01'),
(6, 'Stuber', 231, '2020-09-01'),
(7, 'War Dogs', 122, '2020-09-01'),
(8, 'Central Intelligence', 77, '2020-09-01');

-- --------------------------------------------------------

--
-- Table structure for table `Opinions`
--

CREATE TABLE `Opinions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `is_moderator` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `email`, `password`, `salt`, `is_moderator`) VALUES
(1, 'user@gmail.com', 'user123', 'XXXXXXYYYYYYZZZZZDDDDD', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Cast`
--
ALTER TABLE `Cast`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `Director`
--
ALTER TABLE `Director`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Genre`
--
ALTER TABLE `Genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `MovieDetails`
--
ALTER TABLE `MovieDetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `director_id` (`director_id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `MovieRatings`
--
ALTER TABLE `MovieRatings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `Movies`
--
ALTER TABLE `Movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Opinions`
--
ALTER TABLE `Opinions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Cast`
--
ALTER TABLE `Cast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `Director`
--
ALTER TABLE `Director`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Genre`
--
ALTER TABLE `Genre`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `MovieDetails`
--
ALTER TABLE `MovieDetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `MovieRatings`
--
ALTER TABLE `MovieRatings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `Movies`
--
ALTER TABLE `Movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `Opinions`
--
ALTER TABLE `Opinions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Cast`
--
ALTER TABLE `Cast`
  ADD CONSTRAINT `Cast_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `Movies` (`id`);

--
-- Constraints for table `MovieDetails`
--
ALTER TABLE `MovieDetails`
  ADD CONSTRAINT `MovieDetails_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `Movies` (`id`),
  ADD CONSTRAINT `MovieDetails_ibfk_2` FOREIGN KEY (`director_id`) REFERENCES `Director` (`id`);

--
-- Constraints for table `MovieRatings`
--
ALTER TABLE `MovieRatings`
  ADD CONSTRAINT `MovieRatings_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `Movies` (`id`);

--
-- Constraints for table `Opinions`
--
ALTER TABLE `Opinions`
  ADD CONSTRAINT `Opinions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  ADD CONSTRAINT `Opinions_ibfk_2` FOREIGN KEY (`movie_id`) REFERENCES `Movies` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
